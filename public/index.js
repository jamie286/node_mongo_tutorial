var app = angular.module('test', []);

app.controller('ctrl', function ($scope, $http) {
	$scope.messages = [];
	$scope.message = '';

	$scope.submit = function () {
		$http.post('api/messages', { text: $scope.message })
		.then(function (response) {
			$scope.message = '';
		});
	};

	$scope.get = function () {
		$http.get('api/messages')
		.then(function (response) {
			$scope.messages = response.data;
		});
	};

	$scope.delete = function () {
		$http.delete('api/messages');
	};
});