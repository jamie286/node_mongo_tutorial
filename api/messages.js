var express = require('express');
var router = express.Router();
var Messages = require('./messages.model');

router.post('/', function (req, res) {
	Messages.create(req.body)
	.then(function (message) {
		res.send(message);
	});
});

router.get('/', function (req, res) {
	Messages.find({})
	.then(function (messages) {
		return res.json(messages);
	});
});

router.delete('/', function (req, res) {
	Messages.remove({})
	.then(function () {
		return res.sendStatus(200);
	});
});

module.exports = router;