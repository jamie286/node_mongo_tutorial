var bodyParser = require('body-parser');
var express = require('express');
var http = require('http');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/node_mongo_tutorial');

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api/messages', require('./api/messages'));
app.use(express.static(__dirname + '/public'));

var server = http.createServer(app);
server.listen(9000);

console.log('Listening');